import Vue from 'vue'
import Router from 'vue-router'
import PDashboard from '@/components/hr/pwave/pages/Dashboard'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'PDashboard',
      component: PDashboard
    }
  ]
})
